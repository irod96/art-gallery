from django.contrib import admin

# Register your models here.


from .models import Artes, Artistas

admin.site.register(Artes)
admin.site.register(Artistas)
