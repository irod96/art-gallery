"""parcial2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from galeria import views
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from galeria import views as v

urlpatterns = [
    path('admin/', admin.site.urls),

    path("register/",v.register,name="register"),

    path('',views.index,name="index_view"),

    path('create/', views.create,name="create_view"),
    path('create1/', views.create1,name="create_view1"),
    path('list/', views.list,name="list"),
    path ('detail22/<int:id>',views.detail22,name="detail22"),



        
]
